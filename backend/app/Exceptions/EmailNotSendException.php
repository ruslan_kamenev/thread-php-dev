<?php

declare(strict_types=1);

namespace App\Exceptions;

use http\Exception\RuntimeException;
use Throwable;

final class EmailNotSendException extends RuntimeException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Tweet not found.', $code, $previous);
    }
}
