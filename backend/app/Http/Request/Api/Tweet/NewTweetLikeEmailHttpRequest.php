<?php

namespace App\Http\Request\Api\Tweet;

use Illuminate\Foundation\Http\FormRequest;

class NewTweetLikeEmailHttpRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => 'required|int|min:1'
        ];
    }
}
