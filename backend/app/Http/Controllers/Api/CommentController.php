<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Action\Comment\AddCommentAction;
use App\Action\Comment\AddCommentRequest;
use App\Action\Comment\DeleteCommentAction;
use App\Action\Comment\DeleteCommentRequest;
use App\Action\Comment\GetCommentByIdAction;
use App\Action\Comment\GetCommentCollectionAction;
use App\Action\Comment\GetCommentCollectionByTweetIdAction;
use App\Action\Comment\UpdateCommentAction;
use App\Action\Comment\UpdateCommentRequest;
use App\Action\Comment\UploadCommentImageAction;
use App\Action\Comment\UploadCommentImageRequest;
use App\Action\GetByIdRequest;
use App\Action\GetCollectionRequest;
use App\Exceptions\CommentNotFoundException;
use App\Exceptions\EmailNotSendException;
use App\Exceptions\TweetNotFoundException;
use App\Exceptions\UserNotFoundException;
use App\Http\Controllers\ApiController;
use App\Http\Presenter\CommentAsArrayPresenter;
use App\Http\Request\Api\AddCommentHttpRequest;
use App\Http\Request\Api\Comment\UpdateCommentHttpRequest;
use App\Http\Request\Api\Comment\UploadCommentImageHttpRequest;
use App\Http\Request\Api\Comment\NewCommentLikeEmailHttpRequest;
use App\Http\Response\ApiResponse;
use App\Http\Request\Api\CollectionHttpRequest;
use App\Action\Comment\GetCommentCollectionByTweetIdRequest;
use App\Mail\NewCommentLikeNotification;
use App\Models\Comment;
use App\Models\Tweet;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use TweetTable;

final class CommentController extends ApiController
{
    public function getCommentCollection(
        CollectionHttpRequest $request,
        GetCommentCollectionAction $action,
        CommentAsArrayPresenter $presenter,
    ): ApiResponse {
        $response = $action->execute(
            new GetCollectionRequest(
                (int)$request->query('page'),
                $request->query('sort'),
                $request->query('direction')
            )
        );

        return $this->createPaginatedResponse($response->getPaginator(), $presenter);
    }

    public function getCommentById(
        GetCommentByIdAction $action,
        CommentAsArrayPresenter $presenter,
        string $id,
    ): ApiResponse {
        $comment = $action->execute(new GetByIdRequest((int) $id))->getComment();

        return $this->createSuccessResponse($presenter->present($comment));
    }

    public function newComment(
        AddCommentHttpRequest $request,
        AddCommentAction $action,
        CommentAsArrayPresenter $presenter,
    ): ApiResponse {
        $response = $action->execute(
            new AddCommentRequest(
                $request->get('body'),
                (int)$request->get('tweet_id')
            )
        );

        return $this->created(
            $presenter->present(
                $response->getComment()
            )
        );
    }

    public function getCommentCollectionByTweetId(
        CollectionHttpRequest $request,
        GetCommentCollectionByTweetIdAction $action,
        CommentAsArrayPresenter $presenter,
        string $tweetId,
    ): ApiResponse {
        $response = $action->execute(
            new GetCommentCollectionByTweetIdRequest(
                (int) $tweetId,
                (int) $request->query('page'),
                $request->query('sort'),
                $request->query('direction')
            )
        );

        return $this->createPaginatedResponse($response->getPaginator(), $presenter);
    }

    public function updateCommentById(
        UpdateCommentHttpRequest $request,
        UpdateCommentAction $action,
        CommentAsArrayPresenter $presenter,
        string $id,
    ): ApiResponse {
        $response = $action->execute(
            new UpdateCommentRequest(
                (int)$id,
                $request->get('text')
            )
        );

        return $this->createSuccessResponse(
            $presenter->present(
                $response->getComment()
            )
        );
    }

    public function uploadCommentImage(
        UploadCommentImageHttpRequest $request,
        UploadCommentImageAction $action,
        CommentAsArrayPresenter $presenter,
        string $id,
    ): ApiResponse {
        $response = $action->execute(
            new UploadCommentImageRequest(
                (int)$id,
                $request->file('image')
            )
        );

        return $this->createSuccessResponse(
            $presenter->present(
                $response->getComment()
            )
        );
    }

    public function commentLikedSendEmail (NewCommentLikeEmailHttpRequest $request, int $id) {
        try {
            $comment = Comment::find($id);
        } catch (ModelNotFoundException) {
            throw new CommentNotFoundException();
        }

        try {
            $commentAuthorId = Comment::find($id)->author_id;
            $comment->commentAuthor = User::find($commentAuthorId);
            $comment->likeAuthor = User::find($request->userId);
        } catch (ModelNotFoundException) {
            throw new UserNotFoundException();
        }

        try {
            $emailView = (new NewCommentLikeNotification($comment))->build();
            Mail::to($comment->commentAuthor->email)->send($emailView);
        } catch (EmailNotSendException) {
            throw new EmailNotSendException();
            }
    }

    public function deleteCommentById(
        DeleteCommentAction $action,
        string $id
    ): ApiResponse {
        $action->execute(
            new DeleteCommentRequest(
                (int)$id
            )
        );

        return $this->createDeletedResponse();
    }
}
